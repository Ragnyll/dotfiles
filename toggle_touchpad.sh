#!/bin/zsh

TOUCHPAD_ID=`xinput list | grep "Touchpad" | awk '{print $6}' | sed -e 's/id=//g'`
MOUSE_STATE=`xinput --list-props $TOUCHPAD_ID | grep "Device Enabled" | awk '{print $4}'`

if [ $MOUSE_STATE -eq "1" ]
then
	xinput set-prop $TOUCHPAD_ID "Device Enabled" 0
else
	xinput set-prop $TOUCHPAD_ID "Device Enabled" 1
fi
