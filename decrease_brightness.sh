#!/bin/sh

DISPLAY_NAME="eDP-1"

# Get current brightness
BRIGHTNESS=`xrandr --verbose | grep -m 1 -i brightness | cut -f2 -d " "`
BRIGHTNESS=$(echo "$BRIGHTNESS-.1" | bc)

xrandr --output $DISPLAY_NAME --brightness $BRIGHTNESS
