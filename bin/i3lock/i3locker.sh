#!/bin/sh
# Add a lock icon and text to the center of an image
convert /home/ragnyll/Pictures/backgrounds/nier/blank_menu_dark.png -font Liberation-Sans \
    -pointsize 26 -fill white -gravity center \
    -annotate +0+160 "Locked" /home/ragnyll/Pictures/backgrounds/nier/yorha3_white.png \
    -gravity center -composite newimage.png
