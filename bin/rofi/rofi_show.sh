#!/bin/bash

feh ~/Pictures/backgrounds/nier/blank_menu_dark.png --fullscreen &

FEH_PID=$!

sleep .14

rofi -show run -lines 5

kill -9 $FEH_PID
