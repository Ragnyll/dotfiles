# write out symlinks
ln -sf ~/.dotfiles/config/nvim/init.vim ~/.config/nvim/init.vim
ln -sf ~/.dotfiles/config/bspwm/bspwmrc ~/.config/bspwm/bspwmrc
ln -sf ~/.dotfiles/config/compton.conf ~/.config/compton.conf
ln -sf ~/.dotfiles/config/sxhkd/sxhkdrc ~/.config/sxhkd/sxhkdrc
ln -sf ~/.dotfiles/pythonrc ~/.pythonrc
ln -sf ~/.dotfiles/zshrc ~/.zshrc
ln -sf ~/.dotfiles/ctags ~/.ctags

